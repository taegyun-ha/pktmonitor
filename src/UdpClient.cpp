#include <winsock2.h>
#include <WS2tcpip.h>
#pragma comment(lib,"ws2_32.lib") //Winsock Library

#include "UdpClient.h"

namespace network
{
	std::mutex UdpClient::s_printMtx;

	struct socket_t
	{
		SOCKET socket;
	};

	//////////////////////////////////////////////////////////////
	// WSA  ------------------------------------------------------
	WSA::WSA()
		:m_data(std::make_unique<WSAData>())
	{
		const auto err = WSAStartup(MAKEWORD(2, 2), m_data.get());

		if (err == NO_ERROR)
		{
			m_isInit = true;
		}
		else
		{
			printf("WSAStartup failed with error: %d\n", err);
		}
	}

	WSA::~WSA()
	{
		WSACleanup();
	}


	//////////////////////////////////////////////////////////////
	// UdpClient  ------------------------------------------------
	UdpClient::UdpClient(const uint16_t port, const std::function<void(BufferPtr&&)> addBufferFn)
		: m_port(port), m_addBufferFn(addBufferFn), m_socket(std::make_unique<socket_t>())
	{
		if (WSA::instance().isInit())
		{
			m_socket->socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

			if (m_socket->socket == INVALID_SOCKET)
			{
				printf("socket function failed with error: %u\n", WSAGetLastError());
			}
			else
			{
				m_isInit = bind(port);
			}
		}
	}

	UdpClient::~UdpClient()
	{
		m_isRunning.store(false);

		closesocket(m_socket->socket);

		if (m_recvThread.joinable())
			m_recvThread.join();
	}

	void UdpClient::startReceive()
	{
		if (!m_isRunning.load() && m_isInit && m_addBufferFn)
		{
			m_isRunning.store(true);

			m_recvThread = std::thread([this]()
			{
				while (m_isRunning.load())
				{
					BufferPtr pBuffer = receive();

					if(pBuffer)
						m_addBufferFn(std::move(pBuffer));
				}
			});
		}
	}

	bool UdpClient::bind(const uint16_t port)
	{
		sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = htonl(INADDR_ANY);
		addr.sin_port = htons(port);

		const int ret = ::bind(m_socket->socket, reinterpret_cast<SOCKADDR *>(&addr), sizeof(addr));
		if (ret == SOCKET_ERROR)
		{
			printf("bind failed with error %u\n", WSAGetLastError());
			return false;
		}

		return true;
	}

	BufferPtr UdpClient::receive()
	{
		static constexpr size_t s_bufferSize = 512;

		std::string buffer;

		if (isInit())
		{
			size_t receivedSize{ 0 };
			size_t bufferIdx{ 0 };

			buffer.resize(s_bufferSize);

			do
			{
				/* Allocate enough memory for next input
				 * This will double the size when <buffer> does not have enough memory to 
				 * receive the data */
				if (buffer.size() < s_bufferSize * (bufferIdx + 1))
					buffer.resize(buffer.size() * 2);

				const int ret = recv(m_socket->socket,
					buffer.data() + (s_bufferSize * bufferIdx), // <char*> to store received data
					s_bufferSize, 0);

				if (ret < 0)
					break;

				receivedSize += ret;
				++bufferIdx;

				/* If receivedSize is maximum size it could receive, it's possible that we have not cleared
				 * the buffer yet. Keep receive until the buffer is cleared */
			} while (receivedSize >= s_bufferSize * bufferIdx);

			buffer.resize(receivedSize);

			if (buffer.size() > 0)
			{
				if (getEnablePrint())
				{
					/* We do global lock here to print received message sequentially
					 * This is not an ideal way to handle this and should be handled by log library,
					 * but I will print message in this way for quick implementation */
					std::scoped_lock<std::mutex> lock(s_printMtx);
					printf("%s\n", buffer.c_str());
				}

				// Extract buffer
				auto retBuffer(std::make_unique<Buffer>(buffer));

				if (isValid(retBuffer))
					return retBuffer;
			}
		}

		return nullptr;
	}
} // network
