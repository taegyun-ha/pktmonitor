#pragma once
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <queue>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

#include "Types.h"
#include "Buffer.h"

class System
{
public:
	System();
	~System();

	void init();
	void addBuffer(network::BufferPtr&& pBuffer);
	void setEnablePrintBuffer(const bool bEnable);

private:
	// Main process
	void process();
	void processBuffer(const network::BufferPtr& pBuffer);

	/* getMachines 
	 * This will iterate through the machine names and extract <MachinePtr> from <m_machinePoolSessionless>
	 * when name is found. */
	std::vector<MachinePtr> getMachines(const std::unordered_set<machineId_t>& machineNames);
	MachinePtr getMachine(const machineId_t & machineId, const sessionId_t & sessionId) const;

	// Buffer impl
	void addSession(const network::BufferPtr& pBuffer);
	void addMachine(const network::BufferPtr& pBuffer);
	void updateMachine(const network::BufferPtr& pBuffer);

private:
	std::atomic<bool> m_isAlive{ true };

	std::unordered_map<bufferId_t, std::function<void(const network::BufferPtr&)>> m_bufferImplMap;

	std::unordered_map<port_t, UdpClientPtr> m_udpPool;
	std::unordered_map<sessionId_t, SessionPtr> m_sessionPool;

	// machinePool will hold all exsting machine
	std::unordered_map<machineId_t, MachinePtr> m_machinePool;
	// m_machinePoolSessionless will hold all machine that is not assigned to session
	std::unordered_map<machineId_t, MachinePtr> m_machinePoolSessionless;

	std::queue<network::BufferPtr> m_buffer;
	std::mutex m_bufferMtx;
	std::condition_variable m_bufferCv;

	std::thread m_processThread;
};
