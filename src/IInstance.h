#pragma once
#include <string>

class IInstance
{
public:
	IInstance(const std::string& id)
		: m_id(id)
	{
	}

	inline std::string getId() const { return m_id; }
	virtual std::string getDetails() const = 0;

private:
	const std::string m_id;
};