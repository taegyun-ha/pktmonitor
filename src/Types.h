#pragma once
#include <memory>
#include <string>

namespace network {
	class UdpClient;
};

class Session;
class Machine;

using UdpClientPtr = std::unique_ptr<network::UdpClient>;
using SessionPtr = std::unique_ptr<Session>;
using MachinePtr = std::shared_ptr<Machine>;

using port_t = uint16_t;
using sessionId_t = std::string;
using machineId_t = std::string;
using bufferId_t = std::string;
