#pragma once
#include <unordered_set>
#include "UdpClient.h"

namespace network
{
	/* UdpProtocol 1
	 * - broadcast over UDP port 7106
	 * 
	 * Director sends this on startup:
	 * SESSION2|sessionname|creator|machineid1|machineid2|machineid3|machinen
	 * - First data : "SESSION2"
	 * - Min size   : 3 (SESSION2, sessionname, creator)
	 * 
	 * Actors send this on startup:
	 * MACHINE|machineid|sessionname
	 * - First data : "MACHINE"
	 * - Min size   : 3 (MACHINE, machineid, sessionname) */
	class UdpProtocol1 final : public UdpClient
	{
	public:
		UdpProtocol1(const uint16_t port, const std::function<void(BufferPtr&&)> addBufferFn)
			: UdpClient(port, addBufferFn)
		{
		}

	protected:
		bool isValid(BufferPtr& pBuffer)
		{
			static const std::unordered_set<std::string> s_validBufferId{ "SESSION2", "MACHINE" };
			static constexpr size_t s_minSize = 3;

			const auto& buffer = pBuffer->getData();

			/* Min buffer size : 3
			 * Valid buffer id : SESSION2 , MACHINE */
			if (buffer.size() >= s_minSize && s_validBufferId.count(buffer[0]) > 0)
			{
				return true;
			}

			return false;
		}
	};

	/* UdpProtocol2
	- broadcast over UDP port 7104

	Every machine sends this at least once a second
	MACHINESTATUS|version|fps
	- First data : "MACHINESTATUS"
	- Size       : 3 (MACHINESTATUS, version, fps) */
	class UdpProtocol2 final : public UdpClient
	{
	public:
		UdpProtocol2(const uint16_t port, const std::function<void(BufferPtr&&)> addBufferFn)
			: UdpClient(port, addBufferFn)
		{
		}

	protected:
		bool isValid(BufferPtr& pBuffer)
		{
			static constexpr const char* s_validBufferId = "MACHINESTATUS";
			static constexpr size_t s_minSize = 4;

			const auto& buffer = pBuffer->getData();

			/* Min buffer size : 4
			 * Valid buffer id : MACHINESTATUS */
			if (buffer.size() >= s_minSize && buffer[0] == s_validBufferId)
			{
				return true;
			}

			return false;
		}
	};
} // namespace network
