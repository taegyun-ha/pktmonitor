#include <execution>

#include "UdpProtocol.h"
#include "Session.h"
#include "Machine.h"
#include "System.h"

System::System()
{
	// Prepare buffer impl
	m_bufferImplMap["SESSION2"]      = std::bind(&System::addSession, this, std::placeholders::_1);
	m_bufferImplMap["MACHINE"]       = std::bind(&System::addMachine, this, std::placeholders::_1);
	m_bufferImplMap["MACHINESTATUS"] = std::bind(&System::updateMachine, this, std::placeholders::_1);

	// Prepare udp
	static constexpr port_t protocol1Port = 7106;
	static constexpr port_t protocol2Port = 7104;
	const auto addBufferFn = std::bind(&System::addBuffer, this, std::placeholders::_1);
	m_udpPool[protocol1Port] = std::make_unique<network::UdpProtocol1>(protocol1Port, addBufferFn);
	m_udpPool[protocol2Port] = std::make_unique<network::UdpProtocol2>(protocol2Port, addBufferFn);
}

System::~System()
{
	m_isAlive.store(false);
	m_bufferCv.notify_all();

	if (m_processThread.joinable())
		m_processThread.join();
}

void System::init()
{
	// Init process
	m_processThread = std::thread(&System::process, this);

	// Init udp
	std::for_each(std::execution::par_unseq,
		m_udpPool.begin(), m_udpPool.end(), [](auto& pUdpPortClientPair)
	{
		if(pUdpPortClientPair.second)
			pUdpPortClientPair.second->startReceive();
	});
}

void System::addBuffer(network::BufferPtr && pBuffer)
{
	std::unique_lock<std::mutex> lock(m_bufferMtx);
	m_buffer.push(std::move(pBuffer));
	lock.unlock();

	m_bufferCv.notify_one();
}

void System::setEnablePrintBuffer(const bool bEnable)
{
	for (auto& udpPortPtrPair : m_udpPool)
	{
		if (udpPortPtrPair.second)
			udpPortPtrPair.second->setEnablePrint(bEnable);
	}
}

void System::process()
{
	while (m_isAlive.load())
	{
		std::unique_lock<std::mutex> lock(m_bufferMtx);
		m_bufferCv.wait(lock, [this]()
		{
			return !m_isAlive.load() || m_buffer.size() > 0;
		});

		if (m_isAlive.load())
		{
			// Extract the front buffer from queue, then unlock the mutex
			auto pBuffer = std::move(m_buffer.front());
			m_buffer.pop();
			lock.unlock();

			processBuffer(pBuffer);
		}
	}
}

void System::processBuffer(const network::BufferPtr & pBuffer)
{
	if (pBuffer)
	{
		auto implIt = m_bufferImplMap.find(pBuffer->getId());
		if (implIt != m_bufferImplMap.end())
		{
			implIt->second(pBuffer);
		}
		else
		{
			printf("Invalid id detected %s\n", pBuffer->getId().c_str());
		}
	}
}

std::vector<MachinePtr> System::getMachines(const std::unordered_set<machineId_t>& machineNames)
{
	std::vector<MachinePtr> retMachines;

	for (const auto& machineId : machineNames)
	{
		// Find machine by id from the pool
		auto machineIdPtrPairIt = m_machinePoolSessionless.find(machineId);

		// Machine has been found, move it from pool to <retMachines>
		if (machineIdPtrPairIt != m_machinePoolSessionless.end())
		{
			retMachines.emplace_back(std::move(machineIdPtrPairIt->second));
			m_machinePoolSessionless.erase(machineIdPtrPairIt);
		}
	}

	return retMachines;
}

MachinePtr System::getMachine(const machineId_t & machineId, const sessionId_t & sessionId) const
{
	MachinePtr retMachinePtr;

	auto sessionIdPtrIt = m_sessionPool.find(sessionId);
	if (sessionIdPtrIt != m_sessionPool.end())
	{
		// Session is found, find machine from session
		retMachinePtr = sessionIdPtrIt->second->getMachine(machineId);
	}
	else
	{
		// Session is not found, find machine from the machine pool
		auto machineIdPtrPairIt = m_machinePoolSessionless.find(machineId);
		if (machineIdPtrPairIt != m_machinePoolSessionless.end())
		{
			retMachinePtr = machineIdPtrPairIt->second;
		}
	}

	return retMachinePtr;
}

void System::addSession(const network::BufferPtr & pBuffer)
{
	const auto& buffer = pBuffer->getData();

	const bufferId_t bufferId   = buffer[0];
	const sessionId_t sessionId = buffer[1];
	const std::string creator   = buffer[2];

	auto session = m_sessionPool.insert(std::make_pair(sessionId, nullptr));
	
	// Session id was not found from pool, so we have added id in the pool
	if (session.second)
	{
		// Now we construct actual session
		SessionPtr& sessionPlaceHolder = session.first->second;
		sessionPlaceHolder = std::make_unique<Session>(sessionId, creator);

		// Add all support machine id
		for (size_t bufferIdx = 3; bufferIdx < buffer.size(); ++bufferIdx)
		{
			sessionPlaceHolder->addMachineSupport(buffer[bufferIdx]);
		}

		// Add machines from pool
		auto offlineMachines = sessionPlaceHolder->getOfflineMachines();
		sessionPlaceHolder->addMachines(std::move(getMachines(offlineMachines)));

		printf("New Session [%s] has been added \n", sessionId.c_str());
		printf("%s\n", sessionPlaceHolder->getDetails().c_str());
	}
	else
	{
		printf("Session [%s] already exist in the pool\n", sessionId.c_str());
	}
}

void System::addMachine(const network::BufferPtr & pBuffer)
{
	const auto& buffer = pBuffer->getData();

	const bufferId_t bufferId   = buffer[0];
	const machineId_t machineId = buffer[1];
	const sessionId_t sessionId = buffer[2];

	auto newMachinePtr = std::make_shared<Machine>(machineId, sessionId);

	auto sessionIdPtrPairIt = m_sessionPool.find(sessionId);
	if (sessionIdPtrPairIt != m_sessionPool.end())
	{
		// Session is found, add machine in the session
		auto& rSessionPtr = sessionIdPtrPairIt->second;
		if (rSessionPtr->addMachine(newMachinePtr))
		{
			// Add it in the pool when succeed
			m_machinePool.insert(std::make_pair(machineId, newMachinePtr));
		}

		// Print details
		printf("%s\n", rSessionPtr->getDetails().c_str());
	}
	else
	{
		printf("Session [%s] supporting Machine[%s] not found. Add the machine in the pool\n", sessionId.c_str(), machineId.c_str());
		
		// Session not found, add machine in the machine pool
		m_machinePoolSessionless.insert_or_assign(newMachinePtr->getId(), newMachinePtr);
		m_machinePool.insert_or_assign(newMachinePtr->getId(), newMachinePtr);
	}
}

void System::updateMachine(const network::BufferPtr & pBuffer)
{
	const auto& buffer = pBuffer->getData();

	const bufferId_t bufferId   = buffer[0];
	const machineId_t machineId = buffer[1];
	const std::string version   = buffer[2];
	const std::string fps       = buffer[3];

	auto machineIdPtrPairIt = m_machinePool.find(machineId);
	if (machineIdPtrPairIt != m_machinePool.end() && machineIdPtrPairIt->second)
	{
		machineIdPtrPairIt->second->update(version, fps);
		printf("Update machine[%s]\n", machineId.c_str());
		printf("%s\n", machineIdPtrPairIt->second->getDetails().c_str());
	}
	else
	{
		//printf("Machine[%s] is has not been intialised\n", machineId.c_str());
	}
}
