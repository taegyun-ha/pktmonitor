#pragma once

#include <unordered_map>
#include <unordered_set>

#include <memory>

#include "IInstance.h"
#include "Types.h"

class Session : public IInstance
{
public:
	Session(const sessionId_t& id, const std::string& creator);
	~Session();

	void addMachineSupport(const machineId_t& machineId);
	bool addMachine(MachinePtr& pMachine);
	void addMachines(std::vector<MachinePtr>&& machines);

	// ACCESS
	std::string getDetails() const override;
	inline std::string getCreator() const { return m_creator; }
	inline const std::unordered_set<machineId_t>& getOfflineMachines() const { return m_offlineMachines; }
	MachinePtr getMachine(const machineId_t& machineId);

private:
	const std::string m_creator;

	std::unordered_map<machineId_t, MachinePtr> m_childMachines;
	std::unordered_set<machineId_t> m_offlineMachines;
};
