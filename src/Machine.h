#pragma once
#include <atomic>
#include <chrono>

#include "IInstance.h"
#include "Types.h"

class Machine : public IInstance
{
public:
	static std::string getDefaultDetails(const machineId_t& id, const bool bOnline = false);

public:
	Machine(const machineId_t& id, const sessionId_t& sessionId = "");
	~Machine();

	void update(const std::string& version, const std::string& fps);

	/* TBD: Status can be updated from different thread using timer library, which why I put atomic for <m_isOnline>.
	 * But I will update the online state only when <getDetails> is called for now */
	void updateOnlineStatus() const;

	// ACCESS
	std::string getDetails() const override;
	inline sessionId_t getSession() const { return m_session; }
	inline std::string getVersion() const { return m_version; }
	inline int getFps() const { return m_fps; }
	inline bool getIsOnline() const { return m_isOnline.load(); }

	inline void setSession(const sessionId_t& sessionId) { m_session = sessionId; }

private:
	void setFps(const std::string& fps);

private:
	sessionId_t m_session;
	std::string m_version;
	int m_fps{ 0 };

	std::chrono::steady_clock::time_point m_updateTp;
	mutable std::atomic<bool> m_isOnline{ true };
};
