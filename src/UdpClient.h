#pragma once
#include <atomic>
#include <functional>
#include <thread>
#include <mutex>
#include "Buffer.h"

struct WSAData;

namespace network
{
	struct socket_t;

	/* WSA
	 * This is signleton class initialising winsock. */
	class WSA
	{
	public:
		static WSA& instance()
		{
			static WSA s_instance;
			return s_instance;
		}

		inline bool isInit() const { return m_isInit; }

	private:
		WSA();
		~WSA();
		WSA(const WSA&) = delete;
		WSA& operator=(const WSA&) = delete;

		std::unique_ptr<WSAData> m_data;
		bool m_isInit{ false };
	};

	/* UdpClient
	 * This class will open a udp socket with specified port number.
	 *
	 * Printing received message will be done within <receive()>
	 * This can be enabled/disabled by <setEnablePrint>
	 * - Note : To handle printing asynchronously received message,
	 *          so it can print sequentially, I have put global mutex <s_printMtx>
	 *          This is not an ideal way to handle this and should be handled by log library,
	 *		    but I will print message in this way for quick implementation */
	class UdpClient
	{
		static std::mutex s_printMtx;

	public:
		UdpClient(const uint16_t port, const std::function<void(BufferPtr&&)> addBufferFn);
		~UdpClient();

		void startReceive();
		
		// ACCESS
		inline bool isInit() const { return m_isInit; }
		inline uint16_t getPort() const { return m_port; }
		inline bool getEnablePrint() const { return m_enablePrint; }
	
		inline void setEnablePrint(const bool bEanble) { m_enablePrint.store(bEanble); }

	protected:
		/* This will perform initial validation check before add the received buffer in the main buffer queue
		 * Override this based on protocol type */
		virtual bool isValid(BufferPtr& pBuffer) = 0;

	private:
		bool bind(const uint16_t port);

		/* receive
		 * This will handle undefined size received packet by dynamically allocated string */
		BufferPtr receive();

	private:
		const uint16_t m_port;
		const std::function<void(BufferPtr&&)> m_addBufferFn;
		std::unique_ptr<socket_t> m_socket;
		bool m_isInit{ false };

		std::atomic<bool> m_isRunning{ false };
		std::atomic<bool> m_enablePrint{ false };
		std::thread m_recvThread;
	};
} // network
