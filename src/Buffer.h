#pragma once
#include <vector>
#include <string>
#include <optional>
#include <memory>

namespace network
{
	class Buffer
	{
	public:
		Buffer(const std::string& rBuffer);

		void print() const;

		// ACCESS
		inline const std::vector<std::string>& getData() const { return m_buffer; }
		const std::string getId() const;

	private:
		std::vector<std::string> extract(const std::string& rBuffer);

	private:
		const std::vector<std::string> m_buffer;
	};

	using BufferPtr = std::unique_ptr<Buffer>;

} // network
