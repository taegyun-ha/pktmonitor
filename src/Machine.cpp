#include <sstream>
#include "Machine.h"

std::string Machine::getDefaultDetails(const machineId_t& id, const bool bOnline)
{
	std::stringstream ss;
	ss << "   Machine --------\n";
	ss << "   id     : " << id << std::endl;
	ss << "   status : " << (bOnline ? "online\n" : "offline\n");
	return ss.str();
}

Machine::Machine(const machineId_t & id, const sessionId_t& sessionId)
	: IInstance(id), m_session(sessionId), m_updateTp(std::chrono::steady_clock::now())
{
}

Machine::~Machine()
{
}

void Machine::update(const std::string & version, const std::string & fps)
{
	m_updateTp = std::chrono::steady_clock::now();

	m_version = version;
	setFps(fps);
}

void Machine::updateOnlineStatus() const
{
	// Tick is 1 second
	static const std::chrono::milliseconds s_heartbeatTick(1000);

	/* TBD: Status can be updated from different thread using timer library, which why I put atomic for <m_isOnline>.
	 * But I will update the online state only when <getDetails> is called for now */
	const auto timeFromLastUpdate = std::chrono::steady_clock::now() - m_updateTp;
	if (timeFromLastUpdate > s_heartbeatTick)
		m_isOnline.store(false);
}

std::string Machine::getDetails() const
{
	updateOnlineStatus();

	std::stringstream ss(getDefaultDetails(getId(), getIsOnline()));
	ss << "   version: " << getVersion() << std::endl;
	ss << "   fps    : " << getFps() << std::endl;

	return ss.str();
}

void Machine::setFps(const std::string & fps)
{
	try
	{
		const int fpsi = std::stoi(fps);
		m_fps = fpsi;
	}
	catch (...)
	{
		printf("Invalid fps : %s\n", fps.c_str());
	}
}
