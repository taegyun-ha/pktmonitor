#include <sstream>
#include "Machine.h"
#include "Session.h"

Session::Session(const sessionId_t & id, const std::string & creator)
	: IInstance(id), m_creator(creator)
{
}

Session::~Session()
{
}

void Session::addMachineSupport(const machineId_t& machineId)
{
	if (m_childMachines.insert(std::make_pair(machineId, nullptr)).second)
	{
		printf("Machine [%s] support has been added by the session [%s]\n", machineId.c_str(), getId().c_str());

		// This will be used to find machines from not assigned machine pool from <System>
		m_offlineMachines.insert(machineId);
	}
	else
	{
		printf("Machine [%s] is already supported by the session [%s]\n", machineId.c_str(), getId().c_str());
	}
}

bool Session::addMachine(MachinePtr & pMachine)
{
	if (pMachine)
	{
		auto machineIdPtrPairIt = m_childMachines.find(pMachine->getId());
		if (machineIdPtrPairIt != m_childMachines.end())
		{
			if (getId() == pMachine->getSession())
			{
				// Add machine into our child
				machineIdPtrPairIt->second = std::move(pMachine);

				// Machine has been assigned
				m_offlineMachines.erase(pMachine->getId());

				printf("Machine [%s] has been added in the session [%s]\n", pMachine->getId().c_str(), getId().c_str());

				return true;

			}
			else
			{
				// TBD: Inavalid machine session (not doing anything for now)
				printf("Inavlid session id when adding Machine [%s] : session of machine [%s] expected [%s] \n", pMachine->getId().c_str(), pMachine->getSession().c_str(), getId().c_str());
			}
		}
		else
		{
			printf("Machine [%s] is not supported by session [%s]\n", pMachine->getId().c_str(), getId().c_str());
		}
	}

	return false;
}

void Session::addMachines(std::vector<MachinePtr>&& machines)
{
	for (auto& rMachinePtr : machines)
	{
		if (rMachinePtr)
		{
			// We double check whether machine is supported by the session
			auto machineIdPtrPairIt = m_childMachines.find(rMachinePtr->getId());
			if (machineIdPtrPairIt != m_childMachines.end())
				machineIdPtrPairIt->second = std::move(rMachinePtr);
		}
	}
}

std::string Session::getDetails() const
{
	std::stringstream ss;
	ss << "Session -----------\n";
	ss << "id     : " << getId() << std::endl;
	ss << "creator: " << getCreator() << std::endl;

	for (auto& rMachineIdPtrPair : m_childMachines)
	{
		if (rMachineIdPtrPair.second)
		{
			ss << rMachineIdPtrPair.second->getDetails();
		}
		else
		{
			ss << Machine::getDefaultDetails(rMachineIdPtrPair.first);
		}
	}

	ss << "Session End -------\n";

	return ss.str();
}

MachinePtr Session::getMachine(const machineId_t& machineId)
{
	auto machineIdPtrPairIt = m_childMachines.find(machineId);
	if (machineIdPtrPairIt != m_childMachines.end())
	{
		return machineIdPtrPairIt->second;
	}

	return nullptr;
}
