#include <iostream>
#include <thread>
#include <sstream>
#include "System.h"

static const std::string s_infoMessage = ([]()
{
	std::stringstream ss;
	ss << "Press key [1] to start print buffer input\n";
	ss << "Press key [2] to stop print buffer input\n";
	ss << "Press key [3] finish\n";
	return ss.str();
})();

System s_system;

int main(int argc, char* argv[])
{
	std::cout << s_infoMessage;

	s_system.init();

	// Print input buffer by default
	s_system.setEnablePrintBuffer(true);

	bool alive{ true };
	int keyIn;
	while (alive)
	{
		std::cin >> keyIn;
		switch (keyIn)
		{
		case 1:
			s_system.setEnablePrintBuffer(true);
			break;
		case 2:
			s_system.setEnablePrintBuffer(false);
			break;
		case 3:
			alive = false;
			break;
		default:
			std::cout << s_infoMessage;
			break;
		}
	}

	return 0;
}
