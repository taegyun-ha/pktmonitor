#include <iostream>
#include <sstream>
#include "Buffer.h"

namespace network
{
	Buffer::Buffer(const std::string& rBuffer)
		: m_buffer(extract(rBuffer))
	{

	}

	void Buffer::print() const
	{
		for (const auto& data : m_buffer)
			printf("%s\n", data.c_str());
	}

	const std::string Buffer::getId() const
	{
		if (m_buffer.size() > 0)
			return m_buffer[0];

		return std::string();
	}

	std::vector<std::string> Buffer::extract(const std::string& rBuffer)
	{
		std::vector<std::string> retBuffer;

		if (rBuffer.size() > 0)
		{
			std::istringstream inputBuffer(rBuffer);
			std::string data;

			while (getline(inputBuffer, data, '|'))
			{
				retBuffer.push_back(data);
			}
		}

		return retBuffer;
	}

} // network